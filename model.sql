DROP TABLE IF EXISTS channel;
DROP TABLE IF EXISTS recommendations;
DROP TABLE IF EXISTS videos;


CREATE TABLE channel (
id BIGSERIAL PRIMARY KEY,
title TEXT,
url_img TEXT,
url TEXT UNIQUE,
nb_abo INT,
nb_vues BIGINT DEFAULT NULL,
description TEXT DEFAULT NULL,
pays TEXT DEFAULT NULL,
is_scrap INT DEFAULT 0 CONSTRAINT is_scrap CHECK (is_scrap < 2)
);

CREATE INDEX channel_scrap ON channel (scrap);
CREATE INDEX channel_id ON channel (id);
CREATE INDEX channel_url ON channel (url);


CREATE TABLE recommendations (
id_channel INT,
url_recommande TEXT
);

CREATE INDEX reco_id ON recommendations (id_channel);
CREATE INDEX reco_url ON recommendations (url_recommande);


CREATE TABLE videos (
id_channel INT,
nom TEXT,
url_video TEXT,
duree TEXT,
nb_vues BIGINT
);

CREATE INDEX videos_id_chaine ON videos (id_channel);