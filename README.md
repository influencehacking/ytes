# Scrap & stockage des données de YouTube

## Stockage des données

Les données sont stockés dans un schéma défini dans le fichier model.sql.

On utilisera une base postgre pour le stockage sur un machine virtuelle de l'université.

les accès réseau est la ressource la plus couteuse en temps. YouTube n'accepte pas un nombre indéfini d'accès réseau depuis une même IP.

La gestion de la parralélisation est réalisée au niveau de la BD. Il ne doit pas y avoir deux fois le scrap d'une même URL (variable is_scrap inférieur à 2).

## Scrap des données

Il y a 3 url à scrapper:
- /channels
- /about
- /videos

Deux difficultés:

- Chercher les infos supplémentaire avec des requêtes AJAX pour les pages channels et videos (peut être que 30 videos suffisent)
- Créer un programme stable permettant d'éviter les erreurs réseau ou autre exceptions rencontrées.

## Organisation

-- au 12/03/20 peut encore évoluer

- 1 fichier sql contenant le modèle de données (model.sql).

- 1 fichier python contenant les classes de scrap (scrap.py)

- 1 fichier python contenant la gestion des accès BD

- 1 fichier python à executer pour scraper en boucle