import requests,bs4,re,pg8000

def conv_num(chaine):
    lettre = chaine[len(chaine)-1]
    regex = re.compile(r'([\d,]+).+', re.I)
    num = float(regex.findall(chaine)[0].replace(",","."))
    if lettre == "k":
        return str(int(num*1000))
    elif lettre == "M":
        return str(int(num * 1000000))
    else:
        return str(int(num * 10))

def update_url(url):
    conn = pg8000.connect(user='ubuntu',password='1234',host='172.28.100.39',database='statinfluence')
    cursor = conn.cursor()
    cursor.execute("update url_scrap set scrap = scrap + 1 where url = '" + url +"'")
    conn.commit()
    conn.close()


def requete(url):
    try:
        r = requests.get(url + '/channels')
    except:
        print("Erreur SSL")
        r = requete(url)
    return r

def scrap_channels(url):
    r = requete(url + '/channels')
    soup = bs4.BeautifulSoup(r.text,'html.parser')
    nb_abo = soup.find(id="channels-browse-content-grid").findAll("span",{"class": "yt-subscription-button-subscriber-count-unbranded-horizontal yt-uix-tooltip"})
    liste = soup.find(id="channels-browse-content-grid").findAll('a',{'class': 'yt-ui-ellipsis'})
    title_url = []
    i = 0
    for a in liste:
        title_url.append((a.attrs["title"],a.attrs["href"],conv_num(nb_abo[i].text)))
        i+=1
    return title_url

def scrap_about(url):
    r = requests.get(url + '/about')
    soup = bs4.BeautifulSoup(r.text,'html.parser')
    try:
        nb_vues = int(soup.findAll("span",{"class": "about-stat"})[0].find("b").text.replace("\u202f",""))
    except AttributeError:
        nb_vues = None
        pass
    except IndexError:
        nb_vues = None
        pass
    try:
        description = soup.find("pre").text
    except AttributeError:
        description = None
        pass
    try:
        pays = soup.find("span",{"class": "country-inline"}).text.replace("\n","").replace(" ","")
    except AttributeError:
        pays = None
        pass
    return (nb_vues,description,pays)

def scrap_videos(url):
    r = requests.get(url + '/videos')
    soup = bs4.BeautifulSoup(r.text,'html.parser')
    liste_vues = soup.findAll("ul",{"class": "yt-lockup-meta-info"})
    vues_videos = []
    for i in liste_vues:
        vues_videos.append(i.find("li").text.replace("\u202f","").replace("\xa0vues","").replace('Aucune vue','0'))
    liste_videos = soup.findAll("h3",{"class":"yt-lockup-title"})
    j = 0
    videos = []
    for i in liste_videos:
        try:
            vues_videos[j] = int(vues_videos[j])
        except ValueError:
            vues_videos[j] = None
            pass
        videos.append((i.find("a").attrs["title"],i.find("a").attrs["href"],i.find("span").text.replace("\xa0"," "),vues_videos[j]))
        j+=1
    return videos

def insert_url_bdd(title_urls,id_chaine):
    conn = pg8000.connect(user='ubuntu',password='1234',host='172.28.100.39',database='statinfluence')
    try:
        cursor = conn.cursor()
        cursor.executemany("INSERT INTO url_scrap (title,url,nb_abo) VALUES (%s,%s,%s) ON CONFLICT DO NOTHING",title_urls)
        conn.commit()
    except pg8000.core.IntegrityError:
        conn.commit()
        pass
    cursor.executemany("INSERT INTO recommendations (id_chaine,url_chaine_recommande) VALUES (%s,%s)",recommendations_generator(title_urls,id_chaine))
    conn.commit()
    conn.close()

def recommendations_generator(title_urls,id_chaine):
    for i in title_urls:
        yield(id_chaine,i[1])

def insert_about_bdd(id_chaine,nb_vues,description,pays):
    conn = pg8000.connect(user='ubuntu',password='1234',host='172.28.100.39',database='statinfluence')
    cursor = conn.cursor()
    cursor.execute("UPDATE url_scrap SET nb_vues = %s, description = %s, pays = %s WHERE id = %s",(nb_vues,description,pays,id_chaine))
    conn.commit()
    conn.close()

def insert_videos_bdd(id_chaine,videos):
    conn = pg8000.connect(user='ubuntu',password='1234',host='172.28.100.39',database='statinfluence')
    cursor = conn.cursor()
    cursor.executemany("INSERT INTO videos (id_chaine,nom,url_video,duree,nb_vues) VALUES (%s,%s,%s,%s,%s)",videos_generator(videos,id_chaine))
    conn.commit()
    conn.close()

def videos_generator(videos,id_chaine):
    for i in videos:
        yield(id_chaine,i[0],i[1],i[2],i[3])

def get_random_url():
    conn = pg8000.connect(user='ubuntu',password='1234',host='172.28.100.39',database='statinfluence')
    cursor = conn.cursor()
    cursor.execute("select * from url_scrap where scrap = 0 order by random() limit 1")
    chaine = cursor.fetchone()
    conn.close()
    try:
        update_url(chaine[2])
    except pg8000.core.ProgrammingError:
        chaine = get_random_url()
    print(chaine[1])
    return ("https://www.youtube.com" + chaine[2],chaine[0])

while True:
    url,id_chaine = get_random_url()
    nb_vues,description,pays = scrap_about(url)
    insert_about_bdd(id_chaine,nb_vues,description,pays)
    try:
        videos = scrap_videos(url)
        insert_videos_bdd(id_chaine,videos)
    except AttributeError:
        pass
    except IndexError:
        pass
    except StopIteration:
        pass
    try:
        channels = scrap_channels(url)
        insert_url_bdd(channels,id_chaine)
    except AttributeError:
        pass
    except IndexError:
        pass
