import requests,bs4,re,pg8000


def scrap_img(url):
    r = requests.get(url + '/channels')
    soup = bs4.BeautifulSoup(r.text,'html.parser')
    return soup.find("img",{"class" : "channel-header-profile-image"}).attrs["src"]

def get_url():
    conn = pg8000.connect(user='ubuntu',password='1234',host='172.28.100.39',database='statinfluence')
    cursor = conn.cursor()
    cursor.execute("select id,url from url_scrap where scrap = 1 and url_img is NULL and pays = 'France' limit 1")
    chaine = cursor.fetchone()
    conn.close()
    return ("https://www.youtube.com" + chaine[1],chaine[0])

def update_url_img(url_img,id):
    conn = pg8000.connect(user='ubuntu',password='1234',host='172.28.100.39',database='statinfluence')
    cursor = conn.cursor()
    cursor.execute("update url_scrap set url_img = %s where id = %s",(url_img,id))
    conn.commit()
    conn.close()


while True:
    url,id = get_url()
    print(id)
    try:
        url_img = scrap_img(url)
        update_url_img(url_img,id)
    except AttributeError:
        pass
